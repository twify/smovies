<?php
/**
 * Created by elpiel.
 * Project: smovies
 
 * Date: 1/16/14
 */


namespace Lp\UserBundle\Entity;

use Symfony\Component\Security\Core\User\UserInterface;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 */
class User implements UserInterface
{

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $username;

    // ...
}