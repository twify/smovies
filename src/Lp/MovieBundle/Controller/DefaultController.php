<?php

namespace Lp\MovieBundle\Controller;

use Lp\MovieBundle\Entity\Task;
use Lp\MovieBundle\Form\Type\TaskType;

use Symfony\Component\HttpFoundation\Response;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request; // For Request


class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('LpMovieBundle:Default:index.html.twig', array('name' => $name));
    }

    public function newAction(Request $request)
    {
        //$task = new Task();
        //$form = $this->createForm(new TaskType(), $task);

        // create a task and give it some dummy data for this example
        $task = new Task();

        $form = $this->createFormBuilder($task)
            ->add('task', 'text')
            ->add('dueDate', 'date')
            ->add('save', 'submit')
            ->getForm();

        $form->handleRequest($request);

        if ($form->isValid()) {

            // if( $form->get('save')->isClicked() ) - Checking for particular clicked button if more than one
            // perform some action, such as saving the task to the database

            return $this->redirect($this->generateUrl('task_success'));
        }

        return $this->render('LpMovieBundle:Default:new.html.twig', array(
            'form' => $form->createView(),
        ));

    }

    public function transAction ()
    {
        $translated = $this->get('translator')->trans('Symfony2 is great');

        return new Response( $translated );
    }

}
