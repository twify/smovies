<?php

namespace Lp\MovieBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Lp\MovieBundle\Entity\Movie;
use Lp\MovieBundle\Form\MovieType;

/**
 * Movie controller.
 *
 */
class MovieController extends Controller
{

    /**
     * Lists all Movie entities.
     *
     */
    public function indexAction()
    {
        // geting doctrine enitity manager
        $em = $this->getDoctrine()->getManager();

        //$entities = $em->getRepository('LpMovieBundle:Movie')->findAll();
        //$em = $this->get('doctrine.orm.entity_manager');

        // The DQL to be excecuted for the pagination
        $dql = "SELECT m FROM LpMovieBundle:Movie m";
        $query = $em->createQuery($dql);

        // Loading the paginator
        $paginator = $this->get('knp_paginator');
        // Using the Paginator to create Pagination
        $pagination = $paginator->paginate(
                                $query, /* the DQL created query */
                                $this->get('request')->query->get('page', 1) /* page number */,
                                10/* limit per page */
        );

        // the array that holds Delete forms
        $delete_forms = array();
        // for all pagination entities we create delete form
        foreach ( $pagination as $entity ) {
            $delete_forms[$entity->getId()] = $this->createDeleteForm($entity->getId())->createView();
        }

        // rendering delete forms and entities in Index.html.twif
        return $this->render('LpMovieBundle:Movie:index.html.twig', array(
            //'entities' => $entities,
            'delete_forms' => $delete_forms,
            'pagination' => $pagination,
        ));
    }

    /**
     * Creates a new Movie entity.
     *
     */
    public function newAction(Request $request)
    {
        // Create new entity
        $entity = new Movie();
        // Create the New entity form
        $form = $this->createCreateForm($entity);
        // setting the posted data to the form
        $form->handleRequest($request);

        // If form is valid persist it to the database
        if ($form->isValid()) {
            // Getting Doctrine for the persisting
            $em = $this->getDoctrine()->getManager();

            // Persist the entity
            $em->persist($entity);
            // Save the entity in the Database
            $em->flush();

            // Redirect user to All Movies
            return $this->redirect($this->generateUrl('movie'));
            //return $this->redirect($this->generateUrl('movie_show', array('id' => $entity->getId())));
        }

        // If data is not valid show data in the form
        return $this->render('LpMovieBundle:Movie:new.html.twig', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
    * Creates a form to create a Movie entity.
    *
    * @param Movie $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createCreateForm(Movie $entity)
    {
        $form = $this->createForm(new MovieType(), $entity, array(
            'action' => $this->generateUrl('movie_new'),
            'method' => 'POST',
        ));

        $form->add('submit', 'submit',
            [ 'label' => 'Create' ]
        );

        return $form;
    }

    /**
     * Finds and displays a Movie entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LpMovieBundle:Movie')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Movie entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('LpMovieBundle:Movie:show.html.twig', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView()
        ));
    }

    /**
     * Edits an existing Movie entity.
     *
     */
    public function editAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('LpMovieBundle:Movie')->find($id);

        if ( !$entity ) {
            throw $this->createNotFoundException('Unable to find Movie entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createEditForm($entity);
        $editForm->handleRequest($request);

        if ( $editForm->isValid() ) {
            $em->flush();

            return $this->redirect($this->generateUrl('movie'));
            //return $this->redirect($this->generateUrl('movie_edit', array( 'id' => $id )));
        }

        return $this->render('LpMovieBundle:Movie:edit.html.twig', array(
            'entity' => $entity,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
    * Creates a form to edit a Movie entity.
    *
    * @param Movie $entity The entity
    *
    * @return \Symfony\Component\Form\Form The form
    */
    private function createEditForm(Movie $entity)
    {
        $form = $this->createForm(new MovieType(), $entity, array(
            'action' => $this->generateUrl('movie_edit', array('id' => $entity->getId())),
            'method' => 'PUT',
        ));

        $form->add('submit', 'submit', array('label' => 'Update'));

        return $form;
    }

    /**
     * Deletes a Movie entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->handleRequest($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('LpMovieBundle:Movie')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Movie entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('movie'));
    }

    /**
     * Creates a form to delete a Movie entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('movie_delete', array('id' => $id)))
            ->setMethod('DELETE')
            ->add('submit', 'submit', array('label' => 'Delete'))
            ->getForm()
        ;
    }
}
