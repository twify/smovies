<?php
/**
 * Created by: elpiel
 * Project: smovies
 * 28.12.2013
 */

namespace Lp\MovieBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

/**
 * Category
 *
 * @ORM\Table()
 * @ORM\Table(name="categories")
 * @ORM\Entity(repositoryClass="Lp\MovieBundle\Entity\CategoryRepository")
 */
class Category
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="cat_name", type="string", length=255)
     */
    private $cat_name;

    /**
     * @ORM\ManyToMany(targetEntity="Movie", mappedBy="categories")
     */
    protected $movies;

    public function __construct()
    {
        $this->movies = new ArrayCollection();
    }

    public function __toString()
    {
        return $this->cat_name;
    }


    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     * @return Category
     */
    public function setCatName($name)
    {
        $this->cat_name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getCatName()
    {
        return $this->cat_name;
    }

    /**
     * Add movies
     *
     * @param \Lp\MovieBundle\Entity\Movie $movies
     * @return Category
     */
    public function addMovie(\Lp\MovieBundle\Entity\Movie $movies)
    {
        $this->movies[] = $movies;

        return $this;
    }

    /**
     * Remove movies
     *
     * @param \Lp\MovieBundle\Entity\Movie $movies
     */
    public function removeMovie(\Lp\MovieBundle\Entity\Movie $movies)
    {
        $this->movies->removeElement($movies);
    }

    /**
     * Get movies
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getMovies()
    {
        return $this->movies;
    }
}
