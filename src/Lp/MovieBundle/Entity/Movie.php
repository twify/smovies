<?php
/**
 * Created by: elpiel
 * Project: smovies
 * 13.12.2013
 */

namespace Lp\MovieBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;

// The file uploading handler
use Symfony\Component\HttpFoundation\File\UploadedFile;

/**
 * @ORM\Entity
 * @ORM\Table(name="movies")
 * @ORM\Entity(repositoryClass="Lp\MovieBundle\Entity\MovieRepository")
 * @ORM\HasLifecycleCallbacks()
 */
class Movie
{
    /**
     * @ORM\Column(type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $mv_title;

    /**
     * @ORM\Column(type="text")
     */
    protected $mv_description;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $mv_created_at;

    /**
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $mv_trailer;

    /**
     * The movie poster
     * @ORM\Column(type="string", length=255, nullable=true)
     */
    protected $mv_poster;

    /**
     * The date in which the movie was released
     * @ORM\Column(type="date", nullable=true)
     */
    protected $mv_date;
    /**
     * If the movie was watched or not
     * @ORM\Column(type="boolean")
     */
    protected $mv_watched = FALSE;


    private $file;

    private $temp;

    /**
     * @ORM\ManyToMany(targetEntity="Category", inversedBy="movies")
     */
    private $categories;

    public function __construct()
    {
        $this->categories = new ArrayCollection();
    }

    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set Title
     *
     * @param string $title
     * @return Movie
     */
    public function setMvTitle($title)
    {
        $this->mv_title = $title;

        return $this;
    }

    /**
     * Get MvTitle
     *
     * @return string 
     */
    public function getMvTitle()
    {
        return $this->mv_title;
    }

    /**
     * Set MvDescription
     *
     * @param string $description
     *
     * @return Movie
     */
    public function setMvDescription($description)
    {
        $this->mv_description = $description;

        return $this;
    }

    /**
     * Get MvDescription
     *
     * @return string 
     */
    public function getMvDescription()
    {
        return $this->mv_description;
    }

    /**
     * Set MvCreatedAt before Saving
     * @ORM\PrePersist
     */
    public function setMvCreatedAtValue()
    {
        $this->mv_created_at = new \DateTime();
    }

    /**
     * Set MvCreatedAt
     *
     * @param \DateTime $mv_created_at
     *
     * @return Movie
     */
    public function setMvCreatedAt($mv_created_at)
    {
        $this->mv_created_at = $mv_created_at;

        return $this;
    }

    /**
     * Get MvCreatedAt
     *
     * @return \DateTime 
     */
    public function getMvCreatedAt()
    {
        return $this->mv_created_at;
    }

    /**
     * Set MvTrailer
     *
     * @param string $mv_trailer \Symfony\Component\Validator\Constraints\Url
     *
     * @return Movie
     */
    public function setMvTrailer ($mv_trailer)
    {
        $this->mv_trailer = $mv_trailer;

        return $this;
    }

    /**
     * Get MvTrailer
     *
     * @return string URL
     */
    public function getMvTrailer ()
    {
        return $this->mv_trailer;
    }

    /**
     * Set mvDate
     *
     * @param string $mv_date
     *
     * @return Movie
     */
    public function setMvDate ($mv_date)
    {
        $this->mv_date = $mv_date;

        return $this;
    }

    /**
     * Get mvDate
     *
     * @return string Date
     */
    public function getMvDate ()
    {
        return $this->mv_date;
    }

    /**
     * Set mvWatched
     *
     * @param boolean $mv_watched
     *
     * @return Movie
     */
    public function setMvWatched ($mv_watched)
    {
        $this->mv_watched = $mv_watched;

        return $this;
    }

    /**
     * Get MvWatched
     *
     * @return boolean
     */
    public function getMvWatched ()
    {
        return $this->mv_watched;
    }

    /**
     * Get File.
     *
     * @return UploadedFile
     */
    public function getFile ()
    {
        return $this->file;
    }

    /**
     * Sets File.
     *
     * @param UploadedFile $file
     */
    public function setFile (UploadedFile $file = NULL)
    {
        $this->file = $file;
        // check if we have an old image path
        if ( isset( $this->mv_poster ) ) {
            // store the old name to delete after the update
            $this->temp = $this->mv_poster;
            $this->mv_poster = NULL;
        } else {
            //$this->mv_poster = $file->getClientOriginalName();
            $this->mv_poster = 'initial';
        }
    }

    /**
     * Before Uploading the file and Saving the Entity
     * @ORM\PrePersist()
     * @ORM\PreUpdate()
     */
    public function preUpload ()
    {
        if ( NULL !== $this->getFile() ) {
            // do whatever you want to generate a unique name
            $filename = sha1(uniqid(mt_rand(), TRUE));
            $this->mv_poster = $filename . '.' . $this->getFile()->guessExtension();
        }
    }

    /**
     * Handle the upload of the file
     *
     * @ORM\PostPersist()
     * @ORM\PostUpdate()
     */
    public function upload ()
    {
        if ( NULL === $this->getFile() ) {
            return;
        }

        // if there is an error when moving the file, an exception will
        // be automatically thrown by move(). This will properly prevent
        // the entity from being persisted to the database on error
        $this->getFile()->move($this->getUploadRootDir(), $this->mv_poster);

        // check if we have an old image
        if ( isset( $this->temp ) ) {
            // delete the old image
            unlink($this->getUploadRootDir() . '/' . $this->temp);
            // clear the temp image path
            $this->temp = NULL;
        }
        $this->file = NULL;
    }

    /**
     * On Deleting Movie delete file too
     * @ORM\PostRemove()
     */
    public function removeUpload ()
    {
        if ( $file = $this->getAbsolutePath() ) {
            unlink($file);
        }
    }

    /**
     * Get All Categories
     *
     * @return ArrayCollection
     */

    public function getCategories ()
    {
        return $this->categories;
    }

    /**
     * Add Category
     *
     * @param \Lp\MovieBundle\Entity\Category $category
     * @return Movie
     */
    public function addCategory(\Lp\MovieBundle\Entity\Category $category)
    {
        $this->categories[] = $category;

        return $this;
    }

    /**
     * Remove Category
     *
     * @param \Lp\MovieBundle\Entity\Category $category
     */
    public function removeCategory(\Lp\MovieBundle\Entity\Category $category)
    {
        $this->categories->removeElement($category);
    }

    /**
     * Getting the absolute path of the poster
     *
     * @return null|string
     */
    public function getAbsolutePath ()
    {
        return NULL === $this->mv_poster
            ? NULL
            : $this->getUploadRootDir() . '/' . $this->mv_poster;
    }

    /**
     * Getting the WEB path of the poster
     *
     * @return null|string
     */
    public function getWebPath ()
    {
        return NULL === $this->mv_poster
            ? NULL
            : $this->getUploadDir() . '/' . $this->mv_poster;
    }

    /**
     * Get the absolute path of the uploading dir
     *
     * @return string
     */
    protected function getUploadRootDir ()
    {
        // the absolute directory path where uploaded
        // documents should be saved
        return __DIR__ . '/../../../../web/' . $this->getUploadDir();
    }

    /**
     * Get the relative uploading dir
     *
     * @return string
     */
    protected function getUploadDir ()
    {
        // get rid of the __DIR__ so it doesn't screw up
        // when displaying uploaded doc/image in the view.
        return 'uploads/posters';
    }
}
