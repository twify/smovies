<?php
/**
 * Created by: elpiel
 * Project: smovies
 * 28.12.2013
 */

namespace Lp\MovieBundle\Form\Type;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

/*
 use Symfony\Component\Form\FormInterface; to use in STATIC determineValidationGroups()
 */

class TaskType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder->add('task')
            ->add('dueDate', null, array('widget' => 'single_text'))
            ->add('terms', null, array('mapped' => false))
            ->add('save', 'submit');
            /*
            FOR Step Form use No validation
            ->add('previousStep', 'submit', array(
                'validation_groups' => false,
            ))
             */
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Lp\MovieBundle\Entity\Task',
            'validation_groups' => array('registration'),
            // 'validation_groups' => false, - NO VALIDATION

            /*
            Determine validation group
            'validation_groups' => array(
               'Acme\AcmeBundle\Entity\Client',
               'determineValidationGroups',
            ),
            OR CLOSURE
            'validation_groups' => function(FormInterface $form) {
                $data = $form->getData();
                if (Entity\Client::TYPE_PERSON == $data->getType()) {
                    return array('person');
                } else {
                    return array('company');
                }
            },
             */
        ));
    }

    public function getName()
    {
        return 'task';
    }


}