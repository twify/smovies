<?php

namespace Lp\MovieBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

use Lp\MovieBundle\Entity\Category;

class MovieType extends AbstractType
{
        /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('mv_title')
            ->add('mv_watched', 'checkbox', ['label' => 'Гледан филм', 'required' => FALSE ])
            ->add('mv_description')
            ->add('mv_trailer')
            ->add('file')
            ->add('mv_date', 'date',
                array(
                    'format' => 'dd MMMM yyyy',
                    //'widget' => 'single_text',
                    'years' => range(date('Y')+3, 1950),
                    'empty_value' => array( 'year' => 'Year', 'month' => 'Month', 'day' => 'Day' ),
                )
            )
            //->add('categories', 'entity', new Category())
            ->add('categories', 'entity', array(
                'class' => 'LpMovieBundle:Category',
                'property' => 'cat_name',
                'multiple' => TRUE
            ))
        ;
    }
    
    /**
     * @param OptionsResolverInterface $resolver
     */
    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Lp\MovieBundle\Entity\Movie'
        ));
    }

    /**
     * @return string
     */
    public function getName()
    {
        return 'lp_moviebundle_movie';
    }
}
