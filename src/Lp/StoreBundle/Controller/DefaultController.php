<?php

namespace Lp\StoreBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction($name)
    {
        return $this->render('LpStoreBundle:Default:index.html.twig', array('name' => $name));
    }
}
