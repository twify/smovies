<?php
/**
 * Created by elpiel.
 * Project: smovies
 
 * Date: 12/6/13
 */

namespace Acme\HelloBundle\Controller;

// to use Response
//use Symfony\Component\HttpFoundation\Response;

// To use render from the Main Controller
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class HelloController extends Controller {

    public function indexAction ($name)
    {
        return $this->render(
                    'AcmeHelloBundle:Hello:index.html.twig',
                        array( 'name' => $name )
        );

        //return new Response( '<html><body>Hello ' . $name . '!</body></html>' );
    }

} 